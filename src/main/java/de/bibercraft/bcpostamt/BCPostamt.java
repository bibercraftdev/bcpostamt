/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt;

import de.bibercraft.bcpostamt.command.BCPostamtCommandExecutor;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import de.bibercraft.bccore.BCCorePlugin;
import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.message.BCMessageHandler;
import de.bibercraft.bcpostamt.history.HistoryHandler;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import de.bibercraft.bcpostamt.books.BookHandler;
import de.bibercraft.bcpostamt.command.BCPostamtCommand;
import de.bibercraft.bcpostamt.delivery.DeliveryHandler;
import de.bibercraft.bcpostamt.mailbox.MailboxHandler;
import de.bibercraft.bcpostamt.postamt.PostamtHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 *
 * @author NickDaKing, goasi
 */
public class BCPostamt extends BCCorePlugin {

    private static final Logger logger = Logger.getGlobal();
    private static final API_VERSION min_api_version = API_VERSION._3;
    private DatabaseConnector dbConnector;
    private PostamtHandler postamtHandler;
    private MailboxHandler mailboxHandler;
    private HistoryHandler historyHandler;
    private BookHandler bookHandler;
    private DeliveryHandler deliveryHandler;
    private WorldGuardPlugin worldGuard = null;
    private Economy economy = null;
    private BCPostamtProvider postamtProvider = null;
    public static final String PREFIX = "[BCPostamt] ";

    @Override
    public void onEnable() {
        initCore("BiberCraft Postamt",
                new BCMessageHandler(this, getConfig().getString("language"), PREFIX, ChatColor.GOLD, ChatColor.AQUA, ChatColor.GREEN, ChatColor.RED, ChatColor.AQUA),
                min_api_version);

        try {
            dbConnector = new DatabaseConnector(this);
        } catch (BCDatabaseException e) {
            logger.log(Level.INFO, "Failed to establish database connection - disabling it. Error was: {0}", e.getMessage());
        }
        if (!dbConnector.isConnected()) {
            logger.log(Level.INFO, "{0}Database-Connection failed. Disabling it.", PREFIX);
            getPluginLoader().disablePlugin(this);
        } else {
            if (!setupEconomy()) {
                logger.log(Level.INFO, "{0}No Economy-Plugin found. Disabling it.", PREFIX);
            }
            if (!setupWorldGuard()) {
                logger.log(Level.INFO, "{0}No WorldGuard-Plugin found. Disabling it.", PREFIX);
            }
            bookHandler = new BookHandler(dbConnector, PREFIX);
            historyHandler = new HistoryHandler(this, PREFIX, dbConnector, bookHandler);
            mailboxHandler = new MailboxHandler(this, dbConnector, economy, getConfig(), bookHandler, worldGuard);
            deliveryHandler = new DeliveryHandler(this, dbConnector, mailboxHandler, bookHandler);
            postamtHandler = new PostamtHandler(this, dbConnector, economy, deliveryHandler);
            if (getConfig().getBoolean("enableApi")) {
                postamtProvider = new BCPostamtProvider(deliveryHandler, mailboxHandler);
            }

            this.setCommandExecutor(new BCPostamtCommandExecutor(this, BCPostamtCommand.values()));
            getServer().getPluginManager().registerEvents(new BCPostamtListener(this, economy, postamtHandler, mailboxHandler, deliveryHandler), this);

            logger.log(Level.INFO, "{0}BCPostamt has been enabled", PREFIX);
        }
    }

    @Override
    public void onDisable() {
        logger.log(Level.INFO, "{0}BCPostamt has been disabled", PREFIX);
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            economy = (Economy) economyProvider.getProvider();
        }

        return economy != null;
    }

    private boolean setupWorldGuard() {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            worldGuard = (WorldGuardPlugin) plugin;
        }

        return worldGuard == null;
    }

    public BCPostamtProvider getPostamtProvider() {
        return postamtProvider;
    }

    public PostamtHandler getPostamtHandler() {
        return postamtHandler;
    }

    public MailboxHandler getMailboxHandler() {
        return mailboxHandler;
    }

    public HistoryHandler getHistoryHandler() {
        return historyHandler;
    }

    public DeliveryHandler getDeliveryHandler() {
        return deliveryHandler;
    }
}
