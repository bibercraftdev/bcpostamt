/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.history;

import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.utils.ItemStackSerialization;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.books.BookHandler;
import de.bibercraft.bcpostamt.database.BCPostamtQuery;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author NickDaKing, goasi
 */
public class HistoryHandler {

    private static final Logger logger = Logger.getLogger(HistoryHandler.class.getName());
    private final BCPostamt plugin;
    private final String PREFIX;
    private final SimpleDateFormat dateFormater = new SimpleDateFormat("dd.MM.yy HH:mm");
    private final DatabaseConnector dbConnector;
    private final BookHandler bookHandler;

    public HistoryHandler(BCPostamt plugin, String PREFIX, DatabaseConnector dbConnector, BookHandler bookHandler) {
        this.plugin = plugin;
        this.PREFIX = PREFIX;
        this.dbConnector = dbConnector;
        this.bookHandler = bookHandler;
    }

    public void showFullHistory(CommandSender p, String targetPlayer, int page) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ALL_FROM_DELIVERIES)) {
                prest.setString(1, targetPlayer);
                try (ResultSet rsHistory = prest.executeQuery()) {
                    int startline = 10 * page - 9;
                    if (rsHistory.relative(startline)) {
                        rsHistory.previous();
                        p.sendMessage(ChatColor.GREEN + plugin.getMessageHandler().getTextValue(BCPostamtMessage.S_ID, null) + " "
                                + ChatColor.YELLOW + plugin.getMessageHandler().getTextValue(BCPostamtMessage.RECEIVER, null) + " "
                                + ChatColor.DARK_AQUA + plugin.getMessageHandler().getTextValue(BCPostamtMessage.VIA, null) + " "
                                + ChatColor.BLUE + plugin.getMessageHandler().getTextValue(BCPostamtMessage.SENT, null) + " "
                                + ChatColor.RED + plugin.getMessageHandler().getTextValue(BCPostamtMessage.RECEIVED, null));
                        String ackdateString;
                        String failed = "";
                        Timestamp ackdate;
                        for (int i = 0; i < 10; i++) {
                            if (rsHistory.next()) {
                                ackdate = rsHistory.getTimestamp("ackdate");
                                ackdateString = "-";
                                if (ackdate != null) {
                                    ackdateString = dateFormater.format(ackdate);
                                }
                                if (rsHistory.getBoolean("failed")) {
                                    failed = new StringBuilder().append(ChatColor.DARK_PURPLE).append("!!! ").toString();
                                }
                                p.sendMessage(new StringBuilder().append(ChatColor.GREEN).append("#").append(rsHistory.getString("id")).append(" ").append(failed).append(ChatColor.YELLOW).append(rsHistory.getString("receiver")).append(" ").append(ChatColor.DARK_AQUA).append(rsHistory.getString("via")).append(" ").append(ChatColor.BLUE).append(dateFormater.format(rsHistory.getTimestamp("date"))).append(" ").append(ChatColor.RED).append(ackdateString).toString());
                                failed = "";
                            }
                        }
                    } else {
                        plugin.getMessageHandler().sendInfoMsg(BCPostamtMessage.NO_MORE_ENTRIES, p, null);
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(HistoryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, PREFIX, ex);
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p, null);
        }
    }

    public void showHistoryContent(Player p, String id) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ORIGINALCONTENT_FROM_DELIVERIES)) {
                prest.setString(1, id);
                try (ResultSet rsHistory = prest.executeQuery()) {
                    if (rsHistory.next()) {
                        Inventory deliveryInventory = plugin.getServer().createInventory(p, 27, new StringBuilder().append("Delivery #").append(id).toString());
                        ItemStack[] revertInventory = bookHandler.revertInventory(ItemStackSerialization.deserializeFromBase64(rsHistory.getString("originalcontent")));
                        deliveryInventory.setContents(revertInventory);
                        p.openInventory(deliveryInventory);
                    } else {
                        plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NOT_FOUND, p, null);
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(HistoryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, PREFIX, ex);
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p, null);
        }
    }

    public void showPlayerHistory(Player p, int page) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_SOME_FROM_DELIVERIES)) {
                prest.setString(1, p.getName());
                try (ResultSet rsHistory = prest.executeQuery()) {
                    int startline = 10 * page - 9;
                    p.sendMessage(new StringBuilder().append(PREFIX).append("Sendungsverlauf Seite ").append(page).toString());
                    if (rsHistory.relative(startline)) {
                        rsHistory.previous();
                        p.sendMessage(new StringBuilder().append(ChatColor.YELLOW).append("Empfänger ").append(ChatColor.BLUE).append("Versendet ").append(ChatColor.RED).append("Empfangen ").toString());
                        String ackdateString;
                        String failed = "";
                        Timestamp ackdate;
                        for (int i = 0; i < 10; i++) {
                            if (rsHistory.next()) {
                                ackdate = rsHistory.getTimestamp("ackdate");
                                ackdateString = "-";
                                if (ackdate != null) {
                                    ackdateString = dateFormater.format(ackdate);
                                }
                                if (rsHistory.getBoolean("failed")) {
                                    failed = new StringBuilder().append(ChatColor.DARK_PURPLE).append("!!! ").toString();
                                }
                                p.sendMessage(new StringBuilder().append(failed).append(ChatColor.YELLOW).append(rsHistory.getString("receiver")).append(" ").append(ChatColor.BLUE).append(dateFormater.format(rsHistory.getTimestamp("date"))).append(" ").append(ChatColor.RED).append(ackdateString).toString());
                                failed = "";
                            }
                        }
                    } else {
                        plugin.getMessageHandler().sendInfoMsg(BCPostamtMessage.NO_MORE_ENTRIES, p, null);
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(HistoryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, PREFIX, ex);
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p, null);
        }
    }
}
