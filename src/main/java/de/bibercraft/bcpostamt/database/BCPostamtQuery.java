/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.database;

import de.bibercraft.bccore.io.database.BCQuery;

/**
 *
 * @author NickDaKing, goasi
 */
public enum BCPostamtQuery implements BCQuery {

    SELECT_ID_FROM_POSTAMTS("SELECT id FROM <PREFIX>postamts WHERE location=?", BCQueryType.QUERY),
    INSERT_INTO_POSTAMT("INSERT INTO <PREFIX>postamts (location) VALUES (?)", BCQueryType.MANIPULATION),
    DELETE_FROM_POSTAMT("DELETE FROM <PREFIX>postamts WHERE location=?", BCQueryType.MANIPULATION),
    SELECT_ID_FROM_MAILBOXES_WHERE_LOCATION("SELECT id FROM <PREFIX>mailboxes WHERE location=?", BCQueryType.QUERY),
    SELECT_ID_FROM_MAILBOXES_WHERE_PLAYERNAME("SELECT id FROM <PREFIX>mailboxes WHERE playername=?", BCQueryType.QUERY),
    SEELCT_ID_PLAYERNAME_FROM_MAILBOXES("SELECT id, playername FROM <PREFIX>mailboxes WHERE location=?", BCQueryType.QUERY),
    SELECT_ALL_FROM_MAILBOXES("SELECT id, playername, private FROM <PREFIX>mailboxes WHERE location=?", BCQueryType.QUERY),
    INSERT_INTO_MAILBOXES("INSERT INTO <PREFIX>mailboxes (playername, location, private) VALUES (?, ?, ?)", BCQueryType.MANIPULATION),
    DELETE_FROM_MAILBOXES("DELETE FROM <PREFIX>mailboxes WHERE location=?", BCQueryType.MANIPULATION),
    SELECT_CONTENT_FROM_MAILBOXCONTENT("SELECT content FROM <PREFIX>mailboxcontent WHERE UPPER (playername)= UPPER (?)", BCQueryType.MANIPULATION),
    UPDATE_MAILBOXCONTENT_SET_CONTENT("UPDATE <PREFIX>mailboxcontent SET content=? WHERE UPPER (playername)= UPPER (?)", BCQueryType.MANIPULATION),
    INSERT_INTO_MAILBOXCONTENT("INSERT INTO <PREFIX>mailboxcontent (playername) VALUES (?)", BCQueryType.MANIPULATION),
    SELECT_ID_FROM_DELIVERIES("SELECT id FROM <PREFIX>deliveries WHERE id=?", BCQueryType.QUERY),
    SELECT_ID_CONTENT_FROM_DELIVERIES("SELECT id, content FROM <PREFIX>deliveries WHERE id=?", BCQueryType.QUERY),
    SELECT_ID_SENDER_RECEIVER_FROM_DELIERIES("SELECT id, sender, receiver, date FROM <PREFIX>deliveries WHERE ackdate IS NULL", BCQueryType.QUERY),
    SELECT_ID_SENDER_RECEIVER_FROM_DELIVERIES_WHERE_ID("SELECT id, sender, receiver, date FROM <PREFIX>deliveries WHERE id=?", BCQueryType.QUERY),
    SELECT_ALL_FROM_DELIVERIES("SELECT id, date, receiver, ackdate, failed, via FROM <PREFIX>deliveries WHERE sender=?", BCQueryType.QUERY),
    SELECT_ORIGINALCONTENT_FROM_DELIVERIES("SELECT originalcontent FROM <PREFIX>deliveries WHERE id=?", BCQueryType.QUERY),
    SELECT_SOME_FROM_DELIVERIES("SELECT date, receiver, ackdate, failed FROM <PREFIX>deliveries WHERE sender=?", BCQueryType.QUERY),
    INSERT_INTO_DELIVERIES("INSERT INTO <PREFIX>deliveries (sender, receiver, via, content, originalcontent) VALUES (?, ?, ?, ?, ?);", BCQueryType.MANIPULATION_WITH_RESULT),
    UPDATE_DELVIERIES_SET_CONTENT("UPDATE <PREFIX>deliveries SET content=? WHERE id=?", BCQueryType.MANIPULATION),
    UPDATE_DELVIERIES_SET_ACK("UPDATE <PREFIX>deliveries SET ackdate=? WHERE id=?", BCQueryType.MANIPULATION),
    UPDATE_DELVIERIES_SET_ACKFAILED("UPDATE <PREFIX>deliveries SET ackdate=?, failed = '1' WHERE id=?", BCQueryType.MANIPULATION),
    SELECT_ID_PW_FROM_BOOKS("SELECT id, password FROM <PREFIX>books WHERE hash=?", BCQueryType.QUERY),
    INSERT_INTO_BOOKS("INSERT INTO <PREFIX>books (title, author, text, password, hash) VALUES (?, ?, ?, ?, ?)", BCQueryType.MANIPULATION_WITH_RESULT),
    SELECT_ALL_FROM_BOOKS("SELECT title, author, text FROM <PREFIX>books WHERE id=? AND password=?", BCQueryType.QUERY),
    SELECT_ALL_MAILBOXES("SELECT id, location FROM <PREFIX>mailboxes", BCQueryType.QUERY),
    SELECT_ALL_POSTAMTS("SELECT id, location FROM <PREFIX>postamts", BCQueryType.QUERY);

    private final String raw;
    private final BCQueryType type;

    BCPostamtQuery(String raw, BCQueryType type) {
        this.raw = raw;
        this.type = type;
    }

    @Override
    public String getRawQuery() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BCQueryType getType() {
        return this.type;
    }
}
