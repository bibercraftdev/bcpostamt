/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.database;

import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.io.database.BCMySQLDatabaseIO;
import de.bibercraft.bcpostamt.BCPostamt;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NickDaKing, goasi
 */
public class DatabaseConnector extends BCMySQLDatabaseIO {

    private static final Logger logger = Logger.getLogger(DatabaseConnector.class.getName());
    private static final int version = 1;

    public DatabaseConnector(BCPostamt plugin) throws BCDatabaseException {
        super(plugin,
                plugin.getConfig().getString("prefix"),
                plugin.getConfig().getString("database"),
                plugin.getConfig().getString("username"),
                plugin.getConfig().getString("password"),
                plugin.getConfig().getString("host"),
                plugin.getConfig().getInt("port"));
        this.plugin = plugin;
    }

    @Override
    public void onFirstCreate() {
        try {
            Statement stat = db.getConnection().createStatement();
            stat.executeUpdate("CREATE TABLE IF NOT EXISTS `" + prefix + "books` (`id` mediumint(9) NOT NULL AUTO_INCREMENT,`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`title` varchar(16) NOT NULL,`author` varchar(16) NOT NULL,`text` varchar(12500) NOT NULL,`password` varchar(10) NOT NULL, `hash` binary(16) NOT NULL, PRIMARY KEY (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");
            stat.executeUpdate("CREATE TABLE IF NOT EXISTS `" + prefix + "deliveries` (`id` mediumint(9) NOT NULL AUTO_INCREMENT,`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`sender` varchar(16) NOT NULL,`receiver` varchar(16) NOT NULL,`via` varchar(16) NOT NULL,`content` varchar(2048) NOT NULL,`originalcontent` varchar(2048) NOT NULL,`ackdate` timestamp NULL DEFAULT NULL,`failed` tinyint(1) NOT NULL DEFAULT '0',PRIMARY KEY (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");
            stat.executeUpdate("CREATE TABLE IF NOT EXISTS `" + prefix + "mailboxcontent` (`id` mediumint(9) NOT NULL AUTO_INCREMENT,`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`playername` varchar(16) COLLATE utf8_bin NOT NULL,`content` varchar(16384) COLLATE utf8_bin NOT NULL DEFAULT 'rO0ABXcEAAAAG3BwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcA==',PRIMARY KEY (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");
            stat.executeUpdate("CREATE TABLE IF NOT EXISTS `" + prefix + "mailboxes` (`id` mediumint(9) NOT NULL AUTO_INCREMENT,`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`playername` varchar(16) NOT NULL,`private` tinyint(1) NOT NULL,`location` varchar(200) NOT NULL,PRIMARY KEY (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");
            stat.executeUpdate("CREATE TABLE IF NOT EXISTS `" + prefix + "postamts` (`id` mediumint(9) NOT NULL AUTO_INCREMENT,`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`location` varchar(200) NOT NULL,PRIMARY KEY (`id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX + "Faild to create tables ", ex);
        }
    }

    @Override
    public boolean onDowngrade(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean onUpgrade(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getVersion() {
        return version;
    }
}
