/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.postamt;

import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.utils.LocationConverter;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.database.BCPostamtQuery;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import de.bibercraft.bcpostamt.delivery.DeliveryHandler;
import de.bibercraft.bcpostamt.mailbox.MailboxHandler;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author NickDaKing, goasi
 */
public class PostamtHandler {

    private static final Logger logger = Logger.getLogger(MailboxHandler.class.getName());
    private final BCPostamt plugin;
    private final DatabaseConnector dbConnector;
    private final DeliveryHandler deliveryHandler;
    private final HashMap<Player, Postamt> openPostamtMenus;

    public PostamtHandler(BCPostamt plugin, DatabaseConnector dbConnector, Economy economy, DeliveryHandler deliveryHandler) {
        this.plugin = plugin;
        this.openPostamtMenus = new HashMap<>();
        this.dbConnector = dbConnector;
        this.deliveryHandler = deliveryHandler;
    }

    /**
     * Creates a postamt.
     *
     * @param p the player who is creating the postamt.
     */
    public void createPostamt(Player p) {
        if (p.getTargetBlock(null, 5).getState().getType().equals(Material.WORKBENCH)) {
            try {
                String targetLoc = LocationConverter.getString(p.getTargetBlock(null, 5).getLocation());
                try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_FROM_POSTAMTS)) {
                    prest.setString(1, targetLoc);
                    try (ResultSet rsPostamt = prest.executeQuery()) {
                        if (!rsPostamt.next()) {
                            try (PreparedStatement prestInsert = dbConnector.prepare(BCPostamtQuery.INSERT_INTO_POSTAMT)) {
                                prestInsert.setString(1, targetLoc);
                                prestInsert.execute();
                            }
                            plugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.POSTAMT_CREATED, p);
                        } else {
                            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.POSTAMT_ALREADY_EXISTS, p);
                        }
                    }
                } catch (BCDatabaseException ex) {
                    Logger.getLogger(PostamtHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p);
            }
        } else {
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NO_WORKBENCH, p);
        }
    }

    /**
     * Destroys a postamt.
     *
     * @param p the player who is creating the postamt.
     */
    public void destroyPostamt(Player p) {
        if (p.getTargetBlock(null, 15).getState().getType().equals(Material.WORKBENCH)) {
            try {
                String location = LocationConverter.getString(p.getTargetBlock(null, 10).getLocation());
                PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_FROM_POSTAMTS);
                prest.setString(1, location);
                try (ResultSet rsPostamt = prest.executeQuery()) {
                    if (rsPostamt.next()) {
                        try (PreparedStatement prestDelete = dbConnector.prepare(BCPostamtQuery.DELETE_FROM_POSTAMT)) {
                            prestDelete.setString(1, location);
                            prestDelete.execute();
                        }
                        plugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.POSTAMT_DESTROYED, p);
                    } else {
                        plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.POSTAMT_NOT_EXISTS, p);
                    }
                }
            } catch (SQLException | BCDatabaseException ex) {
                logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p);
            }
        } else {
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NO_WORKBENCH, p);
        }
    }

    /**
     *
     * @param location
     * @return if the block is a postamt.
     */
    public Boolean isPostamt(Location location) {
        try {
            PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_FROM_POSTAMTS);
            prest.setString(1, LocationConverter.getString(location));
            try (ResultSet rsPostamt = prest.executeQuery()) {
                if (rsPostamt.next()) {
                    return true;
                }
            }
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return false;
    }

    /**
     * Opens a postamt for the player.
     *
     * @param player the player
     */
    public void openPostamt(Player player) {
        if (!deliveryHandler.hasPendingDelivery(player.getName()) && !hasOpenPostamt(player)) {
            Inventory postamt = plugin.getServer().createInventory(player, InventoryType.WORKBENCH);
            openPostamtMenus.put(player, new Postamt(postamt));

            postamt.setItem(0, getSendbutton());
            player.openInventory(postamt);
        }
    }

    /**
     * Returns the sendbutton for the postamt.
     *
     * @return the sendbutton for the postamt.
     */
    public ItemStack getSendbutton() {
        ItemStack sendbutton = new ItemStack(Material.MINECART);
        ItemMeta itemMeta = sendbutton.getItemMeta();

        itemMeta.setDisplayName(plugin.getMessageHandler().getTextValue(BCPostamtMessage.SEND));
        List<String> list = new ArrayList<>();
        list.add(plugin.getMessageHandler().getTextValue(BCPostamtMessage.ENTER_RECEIVER));
        itemMeta.setLore(list);

        sendbutton.setItemMeta(itemMeta);

        return sendbutton;
    }

    /**
     * Closes an open postamt.
     *
     * @param player the player
     */
    public void closeOpenPostamt(Player player) {
        Postamt removeOpenPostamt = removeOpenPostamt(player);
        if (removeOpenPostamt != null) {
            player.closeInventory();
        }
    }

    /**
     * removes an open postamt from the list withut closing it.
     *
     * @param player the player.
     * @return the postamt.
     */
    public Postamt removeOpenPostamt(Player player) {
        Postamt get = openPostamtMenus.get(player);
        openPostamtMenus.remove(player);
        return get;
    }

    /**
     * returns the open postamt from the player.
     *
     * @param player the player
     * @return the open postamt from the player
     */
    public Postamt getOpenPostamt(Player player) {
        return openPostamtMenus.get(player);
    }

    /**
     * returns if the player has an open postamt
     *
     * @param player the player
     * @return true, if the player has an open postamt
     */
    public Boolean hasOpenPostamt(Player player) {
        return openPostamtMenus.containsKey(player);
    }

    /**
     * Removes all invalid postkästen and postamts.
     *
     * @param player the player
     */
    public void cleanup(CommandSender player) {
        try {
            ResultSet rs = dbConnector.prepare(BCPostamtQuery.SELECT_ALL_POSTAMTS).executeQuery();
            while (rs.next()) {
                Block block = LocationConverter.getLocation(rs.getString("location"), plugin).getBlock();
                if (!block.getType().equals(Material.WORKBENCH)) {
                    PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.DELETE_FROM_POSTAMT);
                    prest.setString(1, rs.getString("location"));
                    prest.execute();
                }
            }
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, player);
        }
    }
}
