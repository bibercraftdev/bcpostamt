/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.utils;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
/**
 *
 * @author NickDaKing, goasi
 */
public class IconMenu implements Listener {

    private String name;
    private OptionClickEventHandler handler;
    private Plugin plugin;
    private String[] optionNames;
    private ItemStack[] optionIcons;
    private Inventory inventory;
    private Player player;
    private int size;

    public IconMenu(String name, int size, OptionClickEventHandler handler, Plugin plugin, ItemStack[] content, Player player) {
        this.player = player;
        this.name = name;
        this.handler = handler;
        this.plugin = plugin;
        this.size = size;
        this.optionNames = new String[size];
        this.optionIcons = new ItemStack[size];
        this.inventory = Bukkit.createInventory(player, size, name);
        System.arraycopy(content, 0, optionIcons, 0, content.length);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public IconMenu setOption(int position, ItemStack icon, String name, String... info) {
        optionNames[position] = name;
        optionIcons[position] = setItemNameAndLore(icon, name, info);
        return this;
    }

    public ItemStack[] getContent() {
        return inventory.getContents();
    }

    public void open(Player player) {
        this.player = player;
        for (int i = 0; i < optionIcons.length; i++) {
            if (optionIcons[i] != null) {
                inventory.setItem(i, optionIcons[i]);
            }
        }
        player.openInventory(inventory);
    }

    public void close() {
        player.closeInventory();
    }

    public void destroy() {
        HandlerList.unregisterAll(this);
        handler = null;
        optionNames = null;
        optionIcons = null;
    }

    public void setLore(int slot, String newLore) {
        ItemStack is = inventory.getItem(slot);
        if (is != null && is.hasItemMeta()) {
            ItemMeta itemMeta = is.getItemMeta();
            itemMeta.setLore(Arrays.asList(newLore));
            is.setItemMeta(itemMeta);
            inventory.setItem(slot, is);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onInventoryClick(InventoryClickEvent event) {
        Player pl = (Player) event.getWhoClicked();
        if (event.getInventory().getTitle().equals(name) && pl.equals(player)) {
            int slot = event.getRawSlot();
            event.setCancelled(true);

            if (slot >= 0 && slot < size && optionNames != null && optionNames[slot] != null && event.isLeftClick() && event.getCursor().getType().equals(Material.AIR)) {
                OptionClickEvent e = new OptionClickEvent((Player) event.getWhoClicked(), slot, optionNames[slot]);
                if (e.willDestroy()) {
                    destroy();
                    return;
                }
                handler.onOptionClick(e);
                if (e.willClose()) {
                    final Player p = (Player) event.getWhoClicked();
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            p.closeInventory();
                        }
                    }, 1);
                }
            }
        }
    }

    public interface OptionClickEventHandler {

        public void onOptionClick(OptionClickEvent event);
    }

    public class OptionClickEvent {

        private Player player;
        private int position;
        private String name;
        private boolean close;
        private boolean destroy;

        public OptionClickEvent(Player player, int position, String name) {
            this.player = player;
            this.position = position;
            this.name = name;
            this.close = true;
            this.destroy = false;
        }

        public Player getPlayer() {
            return player;
        }

        public int getPosition() {
            return position;
        }

        public String getName() {
            return name;
        }

        public boolean willClose() {
            return close;
        }

        public boolean willDestroy() {
            return destroy;
        }

        public void setWillClose(boolean close) {
            this.close = close;
        }

        public void setWillDestroy(boolean destroy) {
            this.destroy = destroy;
        }
    }

    private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore) {
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(name);
        im.setLore(Arrays.asList(lore));
        item.setItemMeta(im);
        return item;
    }
}
