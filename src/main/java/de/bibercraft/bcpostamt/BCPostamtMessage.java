/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt;

import de.bibercraft.bccore.message.BCMessage;

/**
 *
 * @author NickDaKing, goasi
 */
public enum BCPostamtMessage implements BCMessage {

    DESC_CREATEPOSTKASTEN,
    DESC_DESTROYPOSTKASTEN,
    DESC_HISTORY,
    DESC_INFO,
    DESC_CREATEPOSTAMT,
    DESC_DESTROYPOSTAMT,
    DESC_ADMINHISTORY,
    DESC_SHOWCONTENT,
    DESC_FORCEDELIVERY,
    DESC_OPENPOSTAMT,
    DESC_CLEANUP,
    DESC_SEND,
    DESC_OPENPOSTAMTKASTEN,
    UNKNOWN_ERROR,
    DATABASE_ERROR,
    MAILBOX_CREATED,
    MAILBOX_DESTROYED,
    MAILBOX_ALREADY_EXISTS,
    MAILBOX_NOT_EXISTS,
    MAILBOX_OWNER,
    NOT_A_MAILBOX,
    NO_CHEST,
    NO_WORKBENCH,
    POSTAMT_CREATED,
    POSTAMT_DESTROYED,
    POSTAMT_ALREADY_EXISTS,
    POSTAMT_NOT_EXISTS,
    PACKAGE_SENT,
    NEW_MAIL,
    NOT_ENOUGH_MONEY,
    NOT_YOUR_MAILBOX,
    DELIVERY_FORCED,
    NOT_FOUND,
    ENTER_RECEIVER,
    EMPTY_DELIVERY,
    CLEANUP_SUCCESS,
    TOO_MANY_ITEMS,
    NO_ITEMS,
    PACKAGE,
    LETTER,
    SENDER,
    RECEIVER,
    PACKAGEID,
    DATE,
    GLOBAL_SENDER,
    MAILBOX,
    POSTOFFICE,
    INSTANTDELIVERY,
    PRICE,
    SEND,
    UNPACK,
    UNPACK_TEXT,
    PLAYERS_MAILBOX,
    S_ID,
    VIA,
    SENT,
    RECEIVED,
    NO_MORE_ENTRIES;

    @Override
    public String getId() {
        return this.name().toLowerCase();
    }

}
