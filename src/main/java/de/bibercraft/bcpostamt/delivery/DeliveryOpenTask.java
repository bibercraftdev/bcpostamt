/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.delivery;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author NickDaKing, goasi
 */
public class DeliveryOpenTask extends BukkitRunnable {

    private final Inventory newInventory;
    private final Player player;

    public DeliveryOpenTask(Player player, Inventory newInventory) {
        this.newInventory = newInventory;
        this.player = player;
    }

    @Override
    public void run() {
        player.closeInventory();
        player.openInventory(newInventory);
    }

}
