/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.delivery;

import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.utils.ItemStackSerialization;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtListener;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.books.BookHandler;
import de.bibercraft.bcpostamt.database.BCPostamtQuery;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import de.bibercraft.bcpostamt.mailbox.MailboxHandler;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author NickDaKing, goasi
 */
public class DeliveryHandler {

    private static final Logger logger = Logger.getLogger(DeliveryHandler.class.getName());
    private final BCPostamt plugin;
    private final DatabaseConnector dbConnector;
    private final MailboxHandler mailboxHandler;
    private final BookHandler bookHandler;
    private final DeliveryTask deliveryTask;
    private final HashMap<String, ItemStack[]> pendingDeliveries;
    private final HashMap<Player, DeliveryMenu> openDeliveryMenus;

    public DeliveryHandler(BCPostamt plugin, DatabaseConnector dbConnector, MailboxHandler mailboxHandler, BookHandler bookHandler) {
        this.plugin = plugin;
        this.dbConnector = dbConnector;
        this.mailboxHandler = mailboxHandler;
        this.deliveryTask = new DeliveryTask(dbConnector, plugin);
        this.bookHandler = bookHandler;
        this.pendingDeliveries = new HashMap<>();
        this.openDeliveryMenus = new HashMap<>();

        long delay = 24000 - plugin.getServer().getWorlds().get(0).getTime();
        deliveryTask.runTaskTimerAsynchronously(plugin, delay, 24000);
    }

    public int prepareDelivery(final String source, final String targetPlayer, final String via, final ItemStack[] delivery) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.INSERT_INTO_DELIVERIES)) {
                String content = ItemStackSerialization.serializeToBase64(bookHandler.setShortcuts(delivery));
                prest.setString(1, source);
                prest.setString(2, targetPlayer);
                prest.setString(3, via);
                prest.setString(4, content);
                prest.setString(5, content);
                prest.execute();

                ResultSet generatedKeys = prest.getGeneratedKeys();
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(DeliveryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return -1;
    }

    public ItemStack[] removeButtons(ItemStack[] delivery) {
        ItemStack[] newDelivery = new ItemStack[27];
        System.arraycopy(delivery, 0, newDelivery, 0, 27);
        return newDelivery;
    }

    public void deleteDeliveryFromMailbox(Player player, int id) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_CONTENT_FROM_MAILBOXCONTENT)) {
                prest.setString(1, player.getName());
                try (ResultSet rsMailboxContent = prest.executeQuery()) {
                    if (rsMailboxContent.next()) {
                        Inventory mailboxInventory = plugin.getServer().createInventory(player, 27);
                        ItemStack[] content = ItemStackSerialization.deserializeFromBase64(rsMailboxContent.getString("content"));
                        for (int i = 0; i < content.length; i++) {
                            if (content[i] != null && content[i].hasItemMeta() && content[i].getItemMeta().getLore().get(2).contains(String.valueOf(id))) {
                                content[i] = null;
                                break;
                            }
                        }
                        mailboxInventory.setContents(bookHandler.revertInventory(content));
                        mailboxHandler.saveMailboxInventory(player.getName(), mailboxInventory);
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(DeliveryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BCPostamtListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updatedeliveryContent(Player player, ItemStack[] contents, int deliveryID) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_FROM_DELIVERIES)) {
                prest.setInt(1, deliveryID);
                try (ResultSet rsPackage = prest.executeQuery()) {
                    if (rsPackage.next()) {
                        if (isEmpty(contents)) {
                            deleteDeliveryFromMailbox(player, deliveryID);
                        }
                        String inventory = ItemStackSerialization.serializeToBase64(contents);
                        try (PreparedStatement prestUpdateDeliveries = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_CONTENT)) {
                            prestUpdateDeliveries.setString(1, inventory);
                            prestUpdateDeliveries.setInt(2, deliveryID);
                            prestUpdateDeliveries.execute();
                        } catch (BCDatabaseException ex) {
                            Logger.getLogger(DeliveryHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(DeliveryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
    }

    public boolean isEmpty(ItemStack[] contents) {
        for (ItemStack is : contents) {
            if (is != null) {
                return false;
            }
        }
        return true;
    }

    public Integer getDeliveryId(ItemStack is) {
        if (is != null && is.getType() == Material.CHEST) {
            ItemMeta packageMeta = is.getItemMeta();
            if (packageMeta.hasDisplayName() && packageMeta.getDisplayName().equals(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PACKAGE, null))) {
                List<String> lore = packageMeta.getLore();
                if (lore.size() == 4) {
                    String[] sender = lore.get(0).split(" ");
                    String[] receiver = lore.get(1).split(" ");
                    String[] id = lore.get(2).split(" ");
                    if (sender.length == 2 && receiver.length == 2 && id.length == 2) {
                        if (id[1].matches("[0-9]+")) {
                            return Integer.valueOf(id[1]);
                        }
                    }
                }
            }
        }
        return -1;
    }

    public ItemStack[] getDeliveryContent(Player player, Integer id) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_CONTENT_FROM_DELIVERIES)) {
                prest.setInt(1, id);
                try (ResultSet rsDelivery = prest.executeQuery()) {
                    if (rsDelivery.next()) {
                        return bookHandler.revertInventory(ItemStackSerialization.deserializeFromBase64(rsDelivery.getString("content")));
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(DeliveryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return new ItemStack[27];
    }

    public DeliveryTask getDeliveryTask() {
        return deliveryTask;
    }

    public void addNewPendingDelivery(String playername, ItemStack[] content) {
        if (!hasPendingDelivery(playername)) {
            pendingDeliveries.put(playername, content);
        }
    }

    public ItemStack[] removePendingDelivery(String player) {
        if (hasPendingDelivery(player)) {
            ItemStack[] oldDelivery = pendingDeliveries.get(player);
            ItemStack[] newDelivery = new ItemStack[oldDelivery.length];
            System.arraycopy(oldDelivery, 0, newDelivery, 0, oldDelivery.length);
            pendingDeliveries.remove(player);
            return newDelivery;
        }
        return new ItemStack[0];
    }

    public Boolean hasPendingDelivery(String player) {
        return pendingDeliveries.containsKey(player);
    }

    public void givePendingDeliveryBack(final Player player) {
        ItemStack[] removePendingDelivery = removePendingDelivery(player.getName());
        PlayerInventory inventory = player.getInventory();
        for (ItemStack is : removePendingDelivery) {
            if (is != null) {
                if (!is.hasItemMeta() || !is.getItemMeta().getDisplayName().equals(plugin.getMessageHandler().getTextValue(BCPostamtMessage.SEND, null))) {
                    inventory.addItem(is);
                }
            }
        }
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                player.updateInventory();
            }
        }, 1);
    }

    public void openDeliverymenu(final Player player, final int deliveryID) {
        final DeliveryHandler deliveryHandler = this;
        if (!hasOpenDelivery(player)) {
            DeliveryMenu deliveryMenu = new DeliveryMenu(deliveryID, deliveryHandler, plugin, player);
            openDeliveryMenus.put(player, deliveryMenu);
            deliveryMenu.openMenu();
        }
    }

    public void closeOpenDelivery(Player player) {
        DeliveryMenu removeopenDeliveryMenu = removeOpenDelivery(player);
        if (removeopenDeliveryMenu != null) {
            removeopenDeliveryMenu.close();
        }
    }

    public DeliveryMenu removeOpenDelivery(Player player) {
        DeliveryMenu get = openDeliveryMenus.get(player);
        openDeliveryMenus.remove(player);
        return get;
    }

    public Boolean hasOpenDelivery(Player player) {
        return openDeliveryMenus.containsKey(player);
    }

    /**
     * Orders a instant delivery.
     *
     * @param sender the sender
     * @param receiver the receiver.
     * @param id the itemid
     * @param damage the damagevalue
     * @param amount the amount
     */
    public void instantSend(final String sender, final String receiver, final int id, final short damage, final int amount) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                ItemStack[] iss = new ItemStack[1];
                if (damage > 0) {
                    iss[0] = new ItemStack(Material.getMaterial(id), amount, damage);
                } else {
                    iss[0] = new ItemStack(Material.getMaterial(id), amount);
                }
                int prepareDelivery = prepareDelivery(sender, receiver, plugin.getMessageHandler().getTextValue(BCPostamtMessage.INSTANTDELIVERY, null), iss);
                deliveryTask.deliverOne(prepareDelivery);
            }
        });
    }
}
