/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.delivery;

import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.utils.InventorySerialization;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.database.BCPostamtQuery;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author NickDaKing, goasi
 */
public class DeliveryTask extends BukkitRunnable {

    private final BCPostamt plugin;
    private final DatabaseConnector dbConnector;
    private static final Logger logger = Logger.getLogger(DeliveryTask.class.getName());
    private final int deliveryPeriod;
    private final SimpleDateFormat dateFormater;

    public DeliveryTask(DatabaseConnector dbConnector, BCPostamt plugin) {
        this.dateFormater = new SimpleDateFormat("dd.MM.yy HH:mm");
        this.dbConnector = dbConnector;
        this.plugin = plugin;
        this.deliveryPeriod = plugin.getConfig().getInt("deliveryPeriod") * 86400;
    }

    @Override
    public void run() {
        logger.log(Level.INFO, "{0}Running Delivertask", BCPostamt.PREFIX);
        try (ResultSet rsDeliveries = dbConnector.prepare(BCPostamtQuery.SELECT_ID_SENDER_RECEIVER_FROM_DELIERIES).executeQuery()) {
            Timestamp now = new Timestamp(new Date().getTime());
            long difference;
            ArrayList<Player> notifiedPlayers = new ArrayList<>();
            while (rsDeliveries.next()) {
                difference = (now.getTime() - rsDeliveries.getTimestamp("date").getTime()) / 1000;
                if (difference > 1200) {
                    if (difference > deliveryPeriod || !deliverPackage(rsDeliveries.getInt("id"), rsDeliveries.getString("sender"), rsDeliveries.getString("receiver"), dateFormater.format(now))) {
                        if (deliverPackage(rsDeliveries.getInt("id"), plugin.getConfig().getString("globalsender"), rsDeliveries.getString("sender"), dateFormater.format(now))) {
                            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACK)) {
                                prest.setTimestamp(1, now);
                                prest.setInt(2, rsDeliveries.getInt("id"));
                                prest.execute();
                            }
                        } else {
                            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACKFAILED)) {
                                prest.setTimestamp(1, null);
                                prest.setInt(2, rsDeliveries.getInt("id"));
                                prest.execute();
                            }
                        }
                    } else {
                        try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACK)) {
                            prest.setTimestamp(1, now);
                            prest.setInt(2, rsDeliveries.getInt("id"));
                            prest.execute();
                        }

                        Player receiverPlayer = plugin.getServer().getPlayer(rsDeliveries.getString("receiver"));
                        Boolean exists = false;
                        if (receiverPlayer != null) {
                            for (Player p : notifiedPlayers) {
                                if (p.equals(receiverPlayer)) {
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                notifiedPlayers.add(receiverPlayer);
                                sendPlayerNotice(plugin.getServer().getPlayer(rsDeliveries.getString("receiver")));
                            }
                        }
                    }
                }
            }
            notifiedPlayers.clear();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        } catch (BCDatabaseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    private boolean deliverPackage(int id, String sender, String receiver, String date) {
        closeOpenMailbox(receiver);
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_CONTENT_FROM_MAILBOXCONTENT)) {
                prest.setString(1, receiver);
                try (ResultSet rsMailbox = prest.executeQuery()) {
                    if (rsMailbox.next()) {
                        Inventory mailboxInventory = InventorySerialization.deserializeFromBase64(rsMailbox.getString("content"));
                        if (hasFreeSlot(mailboxInventory)) {
                            mailboxInventory.addItem(generatePackageItemStack(id, sender, receiver, date));
                            try (PreparedStatement prestUpdate = dbConnector.prepare(BCPostamtQuery.UPDATE_MAILBOXCONTENT_SET_CONTENT)) {
                                prestUpdate.setString(1, InventorySerialization.serializeToBase64(mailboxInventory));
                                prestUpdate.setString(2, receiver);
                                prestUpdate.execute();
                                return true;
                            }
                        }
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                }
            } catch (BCDatabaseException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return false;
    }

    private boolean hasFreeSlot(Inventory mailbox) {
        for (ItemStack is : mailbox.getContents()) {
            if (is == null) {
                return true;
            }
        }
        return false;
    }

    private void closeOpenMailbox(String playername) {
        Player player = plugin.getServer().getPlayer(playername);
        if (player != null) {
            if (player.getOpenInventory().getTitle().equalsIgnoreCase(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PLAYERS_MAILBOX, new String[]{playername}))) {
                player.closeInventory();
            }
        }
    }

    private ItemStack generatePackageItemStack(int id, String sender, String receiver, String date) {
        ItemStack is = new ItemStack(Material.CHEST);
        ItemMeta packageMeta = is.getItemMeta();

        packageMeta.setDisplayName(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PACKAGE, null));

        ArrayList<String> lore = new ArrayList<>();
        lore.add(new StringBuilder().append(plugin.getMessageHandler().getTextValue(BCPostamtMessage.SENDER, null)).append(sender).toString());
        lore.add(new StringBuilder().append(plugin.getMessageHandler().getTextValue(BCPostamtMessage.RECEIVER, null)).append(receiver).toString());
        lore.add(new StringBuilder().append(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PACKAGEID, null)).append(id).toString());
        lore.add(new StringBuilder().append(plugin.getMessageHandler().getTextValue(BCPostamtMessage.DATE, null)).append(date).toString());

        packageMeta.setLore(lore);

        is.setItemMeta(packageMeta);

        return is;
    }

    private void sendPlayerNotice(Player player) {
        player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 0);
        plugin.getMessageHandler().sendInfoMsg(BCPostamtMessage.NEW_MAIL, player, null);
    }

    public void deliverAll() {
        Timestamp now = new Timestamp(new Date().getTime());
        ArrayList<Player> notifiedPlayers = new ArrayList<>();
        try {
            try (ResultSet rsDeliveries = dbConnector.prepare(BCPostamtQuery.SELECT_ID_SENDER_RECEIVER_FROM_DELIERIES).executeQuery()) {
                while (rsDeliveries.next()) {
                    if (!deliverPackage(rsDeliveries.getInt("id"), rsDeliveries.getString("sender"), rsDeliveries.getString("receiver"), dateFormater.format(now))) {
                        if (deliverPackage(rsDeliveries.getInt("id"), plugin.getConfig().getString("globalsender"), rsDeliveries.getString("sender"), dateFormater.format(now))) {
                            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACK)) {
                                prest.setTimestamp(1, now);
                                prest.setInt(2, rsDeliveries.getInt("id"));
                                prest.execute();
                            }
                        } else {
                            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACKFAILED)) {
                                prest.setTimestamp(1, null);
                                prest.setInt(2, rsDeliveries.getInt("id"));
                                prest.execute();
                            }
                        }
                    } else {
                        try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACK)) {
                            prest.setTimestamp(1, now);
                            prest.setInt(2, rsDeliveries.getInt("id"));
                            prest.execute();
                        }

                        Player receiverPlayer = plugin.getServer().getPlayer(rsDeliveries.getString("receiver"));
                        Boolean exists = false;
                        if (receiverPlayer != null) {
                            for (Player p : notifiedPlayers) {
                                if (p.equals(receiverPlayer)) {
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                notifiedPlayers.add(receiverPlayer);
                                sendPlayerNotice(plugin.getServer().getPlayer(rsDeliveries.getString("receiver")));
                            }
                        }
                    }
                }
            }
        } catch (BCDatabaseException | SQLException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public void deliverOne(int id) {
        Timestamp now = new Timestamp(new Date().getTime());
        try (PreparedStatement prestQuery = dbConnector.prepare(BCPostamtQuery.SELECT_ID_SENDER_RECEIVER_FROM_DELIVERIES_WHERE_ID)) {
            prestQuery.setInt(1, id);
            ResultSet rsDeliveries = prestQuery.executeQuery();
            if (rsDeliveries.next()) {
                if (!deliverPackage(rsDeliveries.getInt("id"), rsDeliveries.getString("sender"), rsDeliveries.getString("receiver"), dateFormater.format(now))) {
                    if (deliverPackage(rsDeliveries.getInt("id"), plugin.getConfig().getString("globalsender"), rsDeliveries.getString("sender"), dateFormater.format(now))) {
                        try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACK)) {
                            prest.setTimestamp(1, now);
                            prest.setInt(2, rsDeliveries.getInt("id"));
                            prest.execute();
                        }
                    } else {
                        try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACKFAILED)) {
                            prest.setTimestamp(1, null);
                            prest.setInt(2, rsDeliveries.getInt("id"));
                            prest.execute();
                        }
                    }
                } else {
                    try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_DELVIERIES_SET_ACK)) {
                        prest.setTimestamp(1, now);
                        prest.setInt(2, rsDeliveries.getInt("id"));
                        prest.execute();
                    }
                    sendPlayerNotice(plugin.getServer().getPlayer(rsDeliveries.getString("receiver")));
                }
            }
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
}
