/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.delivery;

import de.bibercraft.bcpostamt.utils.IconMenu;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.postamt.PostamtHandler;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author NickDaKing, goasi
 */
public class DeliveryMenu {

    private static final Logger logger = Logger.getLogger(DeliveryMenu.class.getName());
    private final BCPostamt plugin;
    private final Player player;
    private IconMenu menu;
    private final DeliveryHandler deliveryHandler;
    private PostamtHandler postamtHandler;
    private final int deliveryID;

    public DeliveryMenu(int deliveryID, DeliveryHandler deliveryHandler, BCPostamt plugin, Player player) {
        this.deliveryID = deliveryID;
        this.plugin = plugin;
        this.player = player;
        this.deliveryHandler = deliveryHandler;
        createMenu();
    }

    public void openMenu() {
        menu.open(player);
    }

    private void createMenu() {
        String title = new StringBuilder().append(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PACKAGEID, null)).append("#").append(deliveryID).toString();
        menu = new IconMenu(title, 36, new IconMenu.OptionClickEventHandler() {
            @Override
            public void onOptionClick(final IconMenu.OptionClickEvent event) {
                event.setWillClose(false);
                if (event.getName().equals(plugin.getMessageHandler().getTextValue(BCPostamtMessage.UNPACK, null))) {
                    Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                        @Override
                        public void run() {
                            ItemStack[] content = menu.getContent();
                            ItemStack[] oldDelivery = content;
                            ItemStack[] newDelivery = new ItemStack[27];
                            System.arraycopy(oldDelivery, 0, newDelivery, 0, 27);

                            for (int i = 0; i < newDelivery.length; i++) {
                                if (newDelivery[i] != null) {
                                    if (event.getPlayer().getInventory().addItem(newDelivery[i]).isEmpty()) {
                                        newDelivery[i] = null;
                                    }
                                }
                            }

                            deliveryHandler.updatedeliveryContent(player, newDelivery, deliveryID);
                        }
                    });
                    event.getPlayer().closeInventory();
                }
            }
        }, plugin, deliveryHandler.getDeliveryContent(player, deliveryID), player)
                .setOption(35, new ItemStack(Material.IRON_PICKAXE, 1), plugin.getMessageHandler().getTextValue(BCPostamtMessage.UNPACK, null), plugin.getMessageHandler().getTextValue(BCPostamtMessage.UNPACK_TEXT, null));
    }

    public void close() {
        menu.close();
        menu.destroy();
    }
}
