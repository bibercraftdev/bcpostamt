/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt;

import de.bibercraft.bcpostamt.delivery.DeliveryHandler;
import de.bibercraft.bcpostamt.mailbox.MailboxHandler;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author NickDaKing, goasi
 */
public class BCPostamtProvider {

    private final DeliveryHandler deliveryHandler;
    private final MailboxHandler mailboxHandler;

    public BCPostamtProvider(DeliveryHandler deliveryHandler, MailboxHandler mailboxHandler) {
        this.deliveryHandler = deliveryHandler;
        this.mailboxHandler = mailboxHandler;
    }

    /**
     * Prepares a delivery.
     *
     * @param source a string with the source. Can be a playername.
     * @param targetPlayer a string with the target. Must be a playername.
     * @param via a string with the deliverer. Should be the Pluginname. Only visible in logs.
     * @param delivery an ItemStack-array.
     * @return if success
     */
    public Boolean prepareDelivery(String source, String targetPlayer, String via, ItemStack[] delivery) {
        if (mailboxHandler.hasMailbox(targetPlayer)) {
            deliveryHandler.prepareDelivery(source, targetPlayer, via, delivery);
            return true;
        }
        return false;
    }
}
