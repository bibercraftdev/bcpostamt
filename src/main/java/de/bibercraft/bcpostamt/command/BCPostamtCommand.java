/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.command;

import de.bibercraft.bccore.command.BCCommand;
import de.bibercraft.bccore.message.BCMessage;
import de.bibercraft.bccore.permission.BCPermission;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.BCPostamtPermission;

/**
 *
 * @author NickDaKing, goasi
 */
public enum BCPostamtCommand implements BCCommand {

    CREATEPOSTKASTEN("/bcpost create", BCPostamtPermission.CREATEPOSTKASTEN, BCPostamtMessage.DESC_CREATEPOSTKASTEN, 0, 0, true),
    DESTROYPOSTKASTEN("/bcpost destroy", BCPostamtPermission.DESTROYPOSTKASTEN, BCPostamtMessage.DESC_DESTROYPOSTKASTEN, 0, 0, true),
    HISTORY("/bcpost history [Seite]", BCPostamtPermission.HISTORY, BCPostamtMessage.DESC_HISTORY, 0, 1, true),
    INFO("/bcpost info", BCPostamtPermission.INFO, BCPostamtMessage.DESC_INFO, 0, 0, true),
    CREATEPOSTAMT("/bcpa create", BCPostamtPermission.CREATEPOSTAMT, BCPostamtMessage.DESC_CREATEPOSTAMT, 0, 0, true),
    DESTROYPOSTAMT("/bcpa destroy", BCPostamtPermission.DESTROYPOSTAMT, BCPostamtMessage.DESC_DESTROYPOSTAMT, 0, 0, true),
    ADMINHISTORY("/bcpa history <Spieler> [Seite]", BCPostamtPermission.ADMINHISTORY, BCPostamtMessage.DESC_ADMINHISTORY, 1, 2, false),
    SHOWCONTENT("/bcpa showcontent <id>", BCPostamtPermission.SHOWCONTENT, BCPostamtMessage.DESC_SHOWCONTENT, 1, 1, true),
    FORCEDELIVERY("/bcpa forcedelivery", BCPostamtPermission.FORCEDELIVERY, BCPostamtMessage.DESC_FORCEDELIVERY, 0, 0, false),
    OPENPOSTAMT("/bcpa openpostamt", BCPostamtPermission.OPENPOSTAMT, BCPostamtMessage.DESC_OPENPOSTAMT, 0, 0, true),
    CLEANUP("/bcpa cleanup", BCPostamtPermission.CLEANUP, BCPostamtMessage.DESC_CLEANUP, 0, 0, false),
    SEND("/bcpa send <sender> <receiver> <id>:[damage] <amount>", BCPostamtPermission.SEND, BCPostamtMessage.DESC_SEND, 4, 4, false),
    OPENPOSTKASTEN("/bcpost open", BCPostamtPermission.OPENPOSTKASTEN, BCPostamtMessage.DESC_OPENPOSTAMT, 0, 0, true);

    private final String cmd;
    private final int minArgs;
    private final int maxArgs;
    private final BCPostamtPermission perm;
    private final BCPostamtMessage helpMessage;
    private final boolean player;

    BCPostamtCommand(String cmd, BCPostamtPermission perm, BCPostamtMessage helpMessage, int minArgs, int maxArgs, boolean player) {
        this.cmd = cmd;
        this.minArgs = minArgs;
        this.maxArgs = maxArgs;
        this.perm = perm;
        this.helpMessage = helpMessage;
        this.player = player;
    }

    @Override
    public String[] getHeadCommands() {
        return new String[]{"bcs", "spleef"};
    }

    @Override
    public String getCommandHelp() {
        return cmd;
    }

    @Override
    public BCPermission[] getRequiredPermissions() {
        return new BCPermission[]{this.perm};
    }

    @Override
    public BCMessage getHelpMessage() {
        return helpMessage;
    }

    @Override
    public String getName() {
        return this.name().toLowerCase();
    }

    /**
     * @return the minArgs
     */
    @Override
    public int getMinArgs() {
        return minArgs;
    }

    /**
     * @return the maxArgs
     */
    @Override
    public int getMaxArgs() {
        return maxArgs;
    }

    @Override
    public boolean isMustBePlayer() {
        return player;
    }
}
