/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.command;

import de.bibercraft.bccore.BCCorePlugin;
import de.bibercraft.bccore.command.BCCommand;
import de.bibercraft.bccore.command.BCCommandExecutor;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.BCPostamtPermission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author NickDaKing, goasi
 */
public class BCPostamtCommandExecutor extends BCCommandExecutor {

    public BCPostamtCommandExecutor(BCCorePlugin plugin, BCCommand[]  
        ... commands) {
    super(plugin, commands);
    }

    @Override
    public boolean onBCCommand(CommandSender sender, BCCommand cmd, String[] args) {
        BCPostamt postamtPlugin = (BCPostamt) plugin;

        BCPostamtCommand c = (BCPostamtCommand) cmd;

        switch (c) {
            case CREATEPOSTKASTEN:
                String targetplayer;
                if (plugin.getPermissionHandler().checkPermission(BCPostamtPermission.CREATEALLPOSTKASTEN, sender) && args.length > 0) {
                    targetplayer = args[0];
                } else {
                    targetplayer = sender.getName();
                }
                postamtPlugin.getMailboxHandler().createMailbox((Player) sender, targetplayer);
                break;
            case DESTROYPOSTKASTEN:
                postamtPlugin.getMailboxHandler().destroyMailbox((Player) sender);
                break;
            case HISTORY:
                int page = 1;
                if (args.length > 0 && args[0].matches("[0-9]+")) {
                    page = Integer.valueOf(args[0]);
                    if (page < 1) {
                        page = 1;
                    }
                }
                postamtPlugin.getHistoryHandler().showPlayerHistory((Player) sender, page);
                break;
            case INFO:
                postamtPlugin.getMailboxHandler().showMailboxOwner((Player) sender);
                break;
            case CREATEPOSTAMT:
                postamtPlugin.getPostamtHandler().createPostamt((Player) sender);
                break;
            case DESTROYPOSTAMT:
                postamtPlugin.getPostamtHandler().destroyPostamt((Player) sender);
                break;
            case ADMINHISTORY:
                page = 1;
                if (args.length > 0 && args[0].matches("[0-9]+")) {
                    page = Integer.valueOf(args[0]);
                    if (page < 1) {
                        page = 1;
                    }
                }
                postamtPlugin.getHistoryHandler().showFullHistory(sender, args[0], page);
                break;
            case SHOWCONTENT:
                if (args.length > 1 && args[0].matches("[0-9]+")) {
                    postamtPlugin.getHistoryHandler().showHistoryContent((Player) sender, args[0]);
                }
                break;
            case FORCEDELIVERY:
                postamtPlugin.getDeliveryHandler().getDeliveryTask().deliverAll();
                postamtPlugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.DELIVERY_FORCED, sender, null);
                break;
            case OPENPOSTAMT:
                postamtPlugin.getPostamtHandler().openPostamt((Player) sender);
                break;
            case CLEANUP:
                postamtPlugin.getPostamtHandler().cleanup(sender);
                postamtPlugin.getMailboxHandler().cleanup(sender);
                break;
            case SEND:
                String transmitter = args[0];
                String receiver = args[1];
                String[] iddmg = args[2].split(":");
                int id;
                short damage = 0;
                int amount;
                if (!args[3].matches("[0-9]+")) {
                    break;
                }
                if (iddmg.length < 1) {
                    break;
                }
                if (!iddmg[0].matches("[0-9]+")) {
                    break;
                }
                amount = Integer.parseInt(args[3]);
                id = Integer.parseInt(iddmg[0]);
                if (iddmg.length > 1) {
                    if (iddmg[1].matches("[0-9]+")) {
                        damage = Short.parseShort(iddmg[1]);
                    }
                }
                postamtPlugin.getDeliveryHandler().instantSend(transmitter, receiver, id, damage, amount);
                break;
            case OPENPOSTKASTEN:
                postamtPlugin.getMailboxHandler().openMailbox((Player) sender);
                break;
        }
        return true;
    }
}
