/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt;

import de.bibercraft.bccore.permission.BCPermission;

/**
 *
 * @author NickDaKing, goasi
 */
public enum BCPostamtPermission implements BCPermission {

    CREATEPOSTKASTEN("bcpostamt.bcpost.create"),
    CREATEALLPOSTKASTEN("bcpostamt.bcpost.createall"),
    DESTROYPOSTKASTEN("bcpostamt.bcpost.destroy"),
    DESTROYALLPOSTKASTEN("bcpostamt.bcpost.destroyall"),
    USE("bcpostamt.bcpost.use"),
    HISTORY("bcpostamt.bcpost.history"),
    INFO("bcpostamt.bcpost.info"),
    CREATEPOSTAMT("bcpostamt.bcpa.create"),
    DESTROYPOSTAMT("bcpostamt.bcpa.destroy"),
    ADMINHISTORY("bcpostamt.bcpa.history"),
    SHOWCONTENT("bcpostamt.bcpa.showcontent"),
    FORCEDELIVERY("bcpostamt.bcpa.forcedelivery"),
    OPENPOSTAMT("bcpostamt.bcpa.openpostamt"),
    CLEANUP("bcpostamt.bcpa.cleanup"),
    OPENPOSTKASTEN("bcpostamt.bcpa.open"),
    SEND("bcpostamt.bcpa.send"),
    OPENALLPOSTKASTEN("bcpostamt.bcpa.openall");

    private final String fullstring;

    BCPostamtPermission(String fullstring) {
        this.fullstring = fullstring;
    }

    @Override
    public String getFullPermissionString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
