/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.mailbox;

import de.bibercraft.bcpostamt.utils.IconMenu;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import java.util.logging.Logger;
import org.bukkit.entity.Player;

/**
 *
 * @author NickDaKing, goasi
 */
public class MailboxMenu {

    private static final Logger logger = Logger.getLogger(MailboxMenu.class.getName());
    private final BCPostamt plugin;
    private final Player player;
    private IconMenu menu;
    private final MailboxHandler mailboxhandler;

    public MailboxMenu(MailboxHandler mailboxhandler, BCPostamt plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
        this.mailboxhandler = mailboxhandler;
        createMenu();
    }

    public void openMenu() {
        menu.open(player);
    }

    private void createMenu() {
        String title = new StringBuilder().append(player.getName()).append("'s ").append(plugin.getMessageHandler().getTextValue(BCPostamtMessage.MAILBOX, null)).toString();
        menu = new IconMenu(title, 27, new IconMenu.OptionClickEventHandler() {

            @Override
            public void onOptionClick(IconMenu.OptionClickEvent event) {
            }
        }, plugin, mailboxhandler.getMailboxContent(player), player);
    }

    public void close() {
        menu.close();
        menu.destroy();
    }
}
