/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.mailbox;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bccore.message.CoreMessage;
import de.bibercraft.bccore.utils.InventorySerialization;
import de.bibercraft.bccore.utils.ItemStackSerialization;
import de.bibercraft.bccore.utils.LocationConverter;
import de.bibercraft.bcpostamt.BCPostamt;
import de.bibercraft.bcpostamt.BCPostamtMessage;
import de.bibercraft.bcpostamt.BCPostamtPermission;
import de.bibercraft.bcpostamt.books.BookHandler;
import de.bibercraft.bcpostamt.database.BCPostamtQuery;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author NickDaKing, goasi
 */
public class MailboxHandler {

    private static final Logger logger = Logger.getLogger(MailboxHandler.class.getName());
    private final BCPostamt plugin;
    private final DatabaseConnector dbConnector;
    private final FileConfiguration settingsConfig;
    private final BookHandler bookHandler;
    private final Economy economy;
    private final WorldGuardPlugin worldGuard;
    private final HashMap<Player, MailboxMenu> openMailboxMenus;

    public MailboxHandler(BCPostamt plugin, DatabaseConnector dbConnector, Economy economy, FileConfiguration settingsConfig, BookHandler bookHandler, WorldGuardPlugin worldGuard) {
        this.plugin = plugin;
        this.dbConnector = dbConnector;
        this.economy = economy;
        this.bookHandler = bookHandler;
        this.settingsConfig = settingsConfig;
        this.worldGuard = worldGuard;
        this.openMailboxMenus = new HashMap<>();
    }

    public void createMailbox(Player p, String targetPlayer) {
        Boolean isPrivate = true;
        if (targetPlayer.equals("public")) {
            isPrivate = false;
        }
        BlockState targetBlock = p.getTargetBlock(null, 5).getState();
        if (targetBlock.getType() == Material.CHEST) {
            if (worldGuard != null) {
                if (!worldGuard.canBuild(p, targetBlock.getLocation())) {
                    plugin.getCorePlugin().getMessageHandler().sendErrorMsg(CoreMessage.NO_PERMISSION, p);
                    return;
                }
            }
            try {
                if (!isMailbox(targetBlock.getLocation())) {
                    if (economy != null && p.getName().equals(targetPlayer)) {
                        Double price = settingsConfig.getDouble("mailboxSetup");
                        if (economy.has(p.getName(), price)) {
                            economy.withdrawPlayer(p.getName(), price);
                        } else {
                            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NOT_ENOUGH_MONEY, p);
                            return;
                        }
                    }
                    try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.INSERT_INTO_MAILBOXES)) {
                        prest.setString(1, targetPlayer);
                        prest.setString(2, LocationConverter.getString(targetBlock.getLocation()));
                        prest.setInt(3, BooleanUtils.toInteger(isPrivate));
                        prest.execute();
                    } catch (BCDatabaseException ex) {
                        Logger.getLogger(MailboxHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    plugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.MAILBOX_CREATED, p);
                } else {
                    plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.MAILBOX_ALREADY_EXISTS, p);
                }
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p);
            }
        } else {
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NO_CHEST, p);
        }
    }

    public void destroyMailbox(Player p) {
        if (p.getTargetBlock(null, 15).getState().getType() == Material.CHEST) {
            try {
                String location = LocationConverter.getString(p.getTargetBlock(null, 10).getLocation());
                PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SEELCT_ID_PLAYERNAME_FROM_MAILBOXES);
                prest.setString(1, location);
                try (ResultSet rs = prest.executeQuery()) {
                    if (rs.next()) {
                        if (p.getName().equals(rs.getString("playername")) || plugin.getPermissionHandler().checkPermission(BCPostamtPermission.DESTROYALLPOSTKASTEN, p)) {
                            try (PreparedStatement prestDelete = dbConnector.prepare(BCPostamtQuery.DELETE_FROM_MAILBOXES)) {
                                prestDelete.setString(1, location);
                                prestDelete.execute();
                            }
                            plugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.MAILBOX_DESTROYED, p);
                        } else {
                            plugin.getCorePlugin().getMessageHandler().sendErrorMsg(CoreMessage.NO_PERMISSION, p);
                        }
                    } else {
                        plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.MAILBOX_NOT_EXISTS, p);
                    }
                }
            } catch (SQLException | BCDatabaseException ex) {
                logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, p);
            }
        } else {
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NO_CHEST, p);
        }
    }

    public void showMailboxOwner(Player player) {
        if (player.getTargetBlock(null, 15).getState().getType() == Material.CHEST) {
            String owner = getMailboxOwner(player.getTargetBlock(null, 10).getLocation());
            if (owner.isEmpty()) {
                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NOT_A_MAILBOX, player);
            } else {
                plugin.getMessageHandler().sendInfoMsg(BCPostamtMessage.MAILBOX_OWNER, player, new String[]{owner});
            }
        } else {
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NO_CHEST, player);
        }
    }

    public boolean hasMailbox(String playerName) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_FROM_MAILBOXES_WHERE_PLAYERNAME)) {
                prest.setString(1, playerName);
                try (ResultSet rsMailbox = prest.executeQuery()) {
                    if (rsMailbox.next()) {
                        return true;
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(MailboxHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return false;
    }

    public boolean hasMailboxContent(String playerName) {
        try {
            PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_CONTENT_FROM_MAILBOXCONTENT);
            prest.setString(1, playerName);
            try (ResultSet rsMailbox = prest.executeQuery()) {
                if (rsMailbox.next()) {
                    return true;
                }
            }
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return false;
    }

    public Boolean isMailbox(Location location) {
        try {
            PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ID_FROM_MAILBOXES_WHERE_LOCATION);
            prest.setString(1, LocationConverter.getString(location));
            try (ResultSet rsMailbox = prest.executeQuery()) {
                if (rsMailbox.next()) {
                    return true;
                }
            }
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return false;
    }

    public Boolean canUseMailbox(Player player, Location location) {
        return player.getName().equalsIgnoreCase(getMailboxOwner(location)) || "public".equalsIgnoreCase(getMailboxOwner(location));
    }

    public String getMailboxOwner(Location location) {
        try {
            PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SEELCT_ID_PLAYERNAME_FROM_MAILBOXES);
            prest.setString(1, LocationConverter.getString(location));
            try (ResultSet rsMailbox = prest.executeQuery()) {
                if (rsMailbox.next()) {
                    return rsMailbox.getString("playername");
                }
            }
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return "";
    }

    public void saveMailboxInventory(String playername, Inventory inventory) {
        try {
            inventory.setContents(bookHandler.setShortcuts(inventory.getContents()));
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.UPDATE_MAILBOXCONTENT_SET_CONTENT)) {
                prest.setString(1, InventorySerialization.serializeToBase64(inventory));
                prest.setString(2, playername);
                prest.execute();
            } catch (BCDatabaseException ex) {
                Logger.getLogger(MailboxHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
            inventory.clear();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public ItemStack[] getMailboxContent(Player player) {
        String playername = player.getName();
        try {
            PreparedStatement prestSelectContent = dbConnector.prepare(BCPostamtQuery.SELECT_CONTENT_FROM_MAILBOXCONTENT);
            prestSelectContent.setString(1, playername);
            ResultSet rsMailboxContent = prestSelectContent.executeQuery();
            if (rsMailboxContent.next()) {
                return bookHandler.revertInventory(ItemStackSerialization.deserializeFromBase64(rsMailboxContent.getString("content")));
            }
        } catch (SQLException | IOException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return null;
    }

    public void createMailboxInventory(String playername) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_CONTENT_FROM_MAILBOXCONTENT)) {
                prest.setString(1, playername);
                try (ResultSet rs = prest.executeQuery()) {
                    if (!rs.next()) {
                        try (PreparedStatement prestInsert = dbConnector.prepare(BCPostamtQuery.INSERT_INTO_MAILBOXCONTENT)) {
                            prestInsert.setString(1, playername);
                            prestInsert.execute();
                        }
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(MailboxHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
    }

    public Boolean hasNewMail(String playername) {
        try {
            PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_CONTENT_FROM_MAILBOXCONTENT);
            prest.setString(1, playername);
            ResultSet rsMailboxContent = prest.executeQuery();
            if (rsMailboxContent.next()) {
                ItemStack[] contents = InventorySerialization.deserializeFromBase64(rsMailboxContent.getString("content")).getContents();
                for (ItemStack is : contents) {
                    if (is != null && !is.getType().equals(Material.AIR)) {
                        return true;
                    }
                }
            }
        } catch (SQLException | BCDatabaseException | IOException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
        }
        return false;
    }

    public void openMailbox(Player player) {
        if (!hasOpenMailbox(player)) {
            MailboxMenu mailboxMenu = new MailboxMenu(this, plugin, player);
            openMailboxMenus.put(player, mailboxMenu);
            mailboxMenu.openMenu();
        }
    }

    public void closeOpenMailbox(Player player) {
        MailboxMenu removeOpenMailbox = removeOpenMailbox(player);
        if (removeOpenMailbox != null) {
            removeOpenMailbox.close();
        }
    }

    public MailboxMenu removeOpenMailbox(Player player) {
        MailboxMenu get = openMailboxMenus.get(player);
        openMailboxMenus.remove(player);
        return get;
    }

    public Boolean hasOpenMailbox(Player player) {
        return openMailboxMenus.containsKey(player);
    }

    public void cleanup(CommandSender player) {
        try {
            ResultSet rs = dbConnector.prepare(BCPostamtQuery.SELECT_ALL_MAILBOXES).executeQuery();
            while (rs.next()) {
                Block block = LocationConverter.getLocation(rs.getString("location"), plugin).getBlock();
                if (!block.getType().equals(Material.CHEST)) {
                    PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.DELETE_FROM_MAILBOXES);
                    prest.setString(1, rs.getString("location"));
                    prest.execute();
                }
            }
            plugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.CLEANUP_SUCCESS, player);
        } catch (SQLException | BCDatabaseException ex) {
            logger.log(Level.SEVERE, BCPostamt.PREFIX, ex);
            plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.DATABASE_ERROR, player);
        }
    }
}
