/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt;

import de.bibercraft.bccore.message.CoreMessage;
import de.bibercraft.bcpostamt.delivery.DeliveryHandler;
import de.bibercraft.bcpostamt.mailbox.MailboxHandler;
import de.bibercraft.bcpostamt.postamt.Postamt;
import de.bibercraft.bcpostamt.postamt.PostamtHandler;
import java.util.ArrayList;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 *
 * @author NickDaKing, goasi
 */
public class BCPostamtListener implements Listener {

    private final BCPostamt plugin;
    private static final Logger logger = Logger.getLogger(BCPostamtListener.class.getName());
    private final PostamtHandler postamtHandler;
    private final MailboxHandler mailboxHandler;
    private final DeliveryHandler deliveryHandler;
    private final Economy economy;

    public BCPostamtListener(BCPostamt plugin, Economy economy, PostamtHandler postamtHandler, MailboxHandler mailboxHandler, DeliveryHandler deliveryHandler) {
        this.plugin = plugin;
        this.economy = economy;
        this.postamtHandler = postamtHandler;
        this.deliveryHandler = deliveryHandler;
        this.mailboxHandler = mailboxHandler;
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        if (plugin.getPermissionHandler().checkPermission(BCPostamtPermission.USE, player)) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                @Override
                public void run() {
                    if (mailboxHandler.hasNewMail(player.getName())) {
                        plugin.getMessageHandler().sendInfoMsg(BCPostamtMessage.NEW_MAIL, player, null);
                    } else {
                        mailboxHandler.createMailboxInventory(player.getName());
                    }
                }
            });
            postamtHandler.closeOpenPostamt(player);
            mailboxHandler.closeOpenMailbox(player);
            deliveryHandler.closeOpenDelivery(player);
        }
    }

    @EventHandler
    public void onPlayerInteractEvent(final PlayerInteractEvent e) {
        mailboxOpenEvent(e);
        postamtOpenEvent(e);
    }

    @EventHandler
    public void onInventoryCloseEvent(final InventoryCloseEvent e) {
        mailboxCloseEvent(e);
        postamtCloseEvent(e);
        deliveryCloseEvent(e);
    }

    @EventHandler
    public void onPlayerInventoryClickEvent(InventoryClickEvent e) {
        postamtClickEvent(e);
        playerInventoryClickEvent(e);
    }

    @EventHandler
    public void onPlayerChatEvent(final AsyncPlayerChatEvent e) {
        if (deliveryHandler.hasPendingDelivery(e.getPlayer().getName())) {
            e.setCancelled(true);
            final String playername = e.getPlayer().getName();
            if ((economy == null || economy.has(playername, plugin.getConfig().getDouble("price-normal"))) && mailboxHandler.hasMailboxContent(e.getMessage())) {
                Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                    @Override
                    public void run() {
                        ItemStack[] removePendingDelivery = deliveryHandler.removePendingDelivery(playername);
                        deliveryHandler.prepareDelivery(playername, e.getMessage(), "BCPostamt", removePendingDelivery);
                        plugin.getMessageHandler().sendSuccessMsg(BCPostamtMessage.PACKAGE_SENT, e.getPlayer(), null);
                        economy.withdrawPlayer(playername, plugin.getConfig().getDouble("price-normal"));
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.PISTON_EXTEND, 1, 0);
                    }
                });
            } else {
                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.MAILBOX_NOT_EXISTS, e.getPlayer(), null);
                deliveryHandler.givePendingDeliveryBack(e.getPlayer());
            }
        }
    }

    @EventHandler
    public void onBlockDestruction(final BlockBreakEvent e) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                if (e.getBlock().getType().equals(Material.CHEST) && mailboxHandler.isMailbox(e.getBlock().getLocation())) {
                    mailboxHandler.destroyMailbox(e.getPlayer());
                }
                if (e.getBlock().getType().equals(Material.WORKBENCH) && postamtHandler.isPostamt(e.getBlock().getLocation())) {
                    postamtHandler.destroyPostamt(e.getPlayer());
                }
            }
        });
    }

    public void mailboxOpenEvent(PlayerInteractEvent e) {
        Block clickedBlock = e.getClickedBlock();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final Player player = e.getPlayer();
            if (e.getClickedBlock().getType().equals(Material.CHEST) && mailboxHandler.isMailbox(clickedBlock.getLocation())) {
                if (mailboxHandler.canUseMailbox(player, clickedBlock.getLocation())) {
                    Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                        @Override
                        public void run() {
                            mailboxHandler.openMailbox(player);
                        }
                    });
                } else {
                    if (plugin.getPermissionHandler().checkPermission(BCPostamtPermission.USE, player)) {
                        plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NOT_YOUR_MAILBOX, player, null);
                    } else {
                        plugin.getCorePlugin().getMessageHandler().sendErrorMsg(CoreMessage.NO_PERMISSION, player, null);
                    }
                }
                e.setCancelled(true);
            }
        }
    }

    public void postamtOpenEvent(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final Player player = e.getPlayer();
            if (e.getClickedBlock().getType().equals(Material.WORKBENCH) && postamtHandler.isPostamt(e.getClickedBlock().getLocation())) {
                if (plugin.getPermissionHandler().checkPermission(BCPostamtPermission.USE, player)) {
                    Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                        @Override
                        public void run() {
                            postamtHandler.openPostamt(player);
                        }
                    });
                } else {
                    plugin.getCorePlugin().getMessageHandler().sendErrorMsg(CoreMessage.NO_PERMISSION, player, null);
                }
                e.setCancelled(true);
            }
        }
    }

    public void mailboxCloseEvent(final InventoryCloseEvent e) {
        if (e.getInventory().getName().contains(plugin.getMessageHandler().getTextValue(BCPostamtMessage.MAILBOX, null)) && mailboxHandler.hasOpenMailbox((Player) e.getPlayer())) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                @Override
                public void run() {
                    mailboxHandler.closeOpenMailbox((Player) e.getPlayer());
                }
            });
        }
    }

    public void postamtCloseEvent(InventoryCloseEvent e) {
        final Player player = (Player) e.getPlayer();
        if (postamtHandler.hasOpenPostamt(player)) {
            Postamt removeOpenPostamt = postamtHandler.removeOpenPostamt(player);
            if (!removeOpenPostamt.isSent()) {
                PlayerInventory inventory = player.getInventory();
                for (ItemStack is : e.getInventory().getContents()) {
                    if (is != null && (!is.hasItemMeta() || !(is.getItemMeta().hasDisplayName() && is.getItemMeta().getDisplayName().equals(plugin.getMessageHandler().getTextValue(BCPostamtMessage.SEND, null))))) {
                        inventory.addItem(is);
                    }
                }
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        player.updateInventory();
                    }
                }, 1);
            }
        }
    }

    public void deliveryCloseEvent(InventoryCloseEvent e) {
        final Player player = (Player) e.getPlayer();
        if (deliveryHandler.hasOpenDelivery(player) && e.getInventory().getTitle().contains(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PACKAGEID, null))) {
            deliveryHandler.closeOpenDelivery(player);
        }
    }

    public void playerInventoryClickEvent(InventoryClickEvent e) {
        if (e.isLeftClick() && e.getInventory().getName().equals(plugin.getMessageHandler().getTextValue(BCPostamtMessage.PLAYERS_MAILBOX, new String[]{e.getWhoClicked().getName()})) && e.getWhoClicked() instanceof Player) {
            final ItemStack currentItem = e.getCurrentItem();
            final Player player = (Player) e.getWhoClicked();
            Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                @Override
                public void run() {
                    int deliveryID = deliveryHandler.getDeliveryId(currentItem);
                    if (deliveryID != -1) {
                        mailboxHandler.closeOpenMailbox(player);
                        deliveryHandler.openDeliverymenu(player, deliveryID);
                    }
                }
            });
            e.setCancelled(true);
        }
    }

    public void postamtClickEvent(InventoryClickEvent e) {
        final Player player = (Player) e.getWhoClicked();
        if (postamtHandler.hasOpenPostamt(player)) {
            e.getInventory().setItem(0, postamtHandler.getSendbutton());
            if (e.getSlotType().equals(SlotType.RESULT)) {
                e.setCancelled(true);
                Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<ItemStack> content = new ArrayList<>();
                        for (ItemStack is : postamtHandler.getOpenPostamt(player).getPostamtInventory().getContents()) {
                            if (is != null && (!is.hasItemMeta() || !(is.getItemMeta().hasDisplayName() && is.getItemMeta().getDisplayName().equals(plugin.getMessageHandler().getTextValue(BCPostamtMessage.SEND, null))))) {
                                content.add(is);
                            }
                        }
                        if (economy != null) {
                            if (!economy.has(player.getName(), plugin.getConfig().getDouble("price-normal"))) {
                                plugin.getMessageHandler().sendErrorMsg(BCPostamtMessage.NOT_ENOUGH_MONEY, player, null);
                                player.closeInventory();
                                return;
                            }
                        }
                        ItemStack[] aContent = new ItemStack[content.size()];
                        for (int i = 0; i < aContent.length; i++) {
                            aContent[i] = content.get(i);
                        }
                        postamtHandler.getOpenPostamt(player).setSent(true);
                        player.closeInventory();
                        deliveryHandler.addNewPendingDelivery(player.getName(), aContent);
                        plugin.getMessageHandler().sendInfoMsg(BCPostamtMessage.ENTER_RECEIVER, player, null);
                    }
                });
                return;
            }
            //Cancel Shift-Click in custominventory because BUKKIT-1929
            if (e.isShiftClick()) {
                e.setCancelled(true);
            }
            Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
                @Override
                public void run() {
                    player.updateInventory();
                }
            });
        }
    }
}
