/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.books;

import de.bibercraft.bccore.io.database.BCDatabaseException;
import de.bibercraft.bcpostamt.BCPostamtListener;
import de.bibercraft.bcpostamt.database.DatabaseConnector;
import de.bibercraft.bcpostamt.database.BCPostamtQuery;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author NickDaKing, goasi
 */
public class BookHandler {

    private final DatabaseConnector dbConnector;
    private final String PREFIX;
    private static final Logger logger = Logger.getLogger(BCPostamtListener.class.getName());
    private final SecureRandom random = new SecureRandom();

    public BookHandler(DatabaseConnector dbConnenctor, String PREFIX) {
        this.dbConnector = dbConnenctor;
        this.PREFIX = PREFIX;
    }

    public ItemStack[] setShortcuts(ItemStack[] iss) {
        for (int i = 0; i < iss.length; i++) {
            ItemStack is = iss[i];
            if (is != null && is.getType().equals(Material.WRITTEN_BOOK)) {
                String key = submitBook(is);
                iss[i] = setItemStackName(new ItemStack(Material.BED), key);
            }
        }
        return iss;
    }

    public ItemStack[] revertInventory(ItemStack[] iss) {
        ItemStack is;
        for (int i = 0; i < iss.length; i++) {
            is = iss[i];
            if (is != null && is.getType().equals(Material.BED)) {
                Book book = null;
                if (is.hasItemMeta() && is.getItemMeta().hasDisplayName()) {
                    book = getBook(is.getItemMeta().getDisplayName());
                }
                if (book != null) {
                    iss[i] = book.generateItemStack(1);
                }
            }
        }
        return iss;
    }

    private String submitBook(ItemStack is) {
        try {
            Book book = new Book(is);
            String title = book.getTitle();
            String author = book.getAuthor();
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] bytesOfMessage = new StringBuilder().append(title).append(":").append(author).append(":").append(book.getText()).toString().getBytes("UTF-8");
            byte[] thedigest = md.digest(bytesOfMessage);
            try (PreparedStatement prestLookup = dbConnector.prepare(BCPostamtQuery.SELECT_ID_PW_FROM_BOOKS)) {
                prestLookup.setBytes(1, thedigest);
                try (ResultSet rs1 = prestLookup.executeQuery()) {
                    if (!rs1.next()) {
                        String key = new BigInteger(50, random).toString(32);
                        try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.INSERT_INTO_BOOKS)) {
                            prest.setString(1, title);
                            prest.setString(2, author);
                            prest.setString(3, book.getText());
                            prest.setString(4, key);
                            prest.setBytes(5, thedigest);
                            prest.execute();
                        }

                        try (ResultSet rs2 = prestLookup.executeQuery()) {
                            if (rs2.next()) {
                                return new StringBuilder().append(rs2.getInt("id")).append(":").append(key).toString();
                            }
                        }
                    } else {
                        return new StringBuilder().append(rs1.getInt("id")).append(":").append(rs1.getString("password")).toString();
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(BookHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, PREFIX, ex);
        }
        return null;
    }

    private Book getBook(String key) {
        try {
            try (PreparedStatement prest = dbConnector.prepare(BCPostamtQuery.SELECT_ALL_FROM_BOOKS)) {
                String[] split = key.split(":");
                if (split.length > 1) {
                    prest.setString(1, split[0]);
                    prest.setString(2, split[1]);
                    try (ResultSet rs = prest.executeQuery()) {
                        if (rs.next()) {
                            return new Book(rs.getString("title"), rs.getString("author"), Arrays.asList(rs.getString("text").split("<!>")));
                        }
                    }
                }
            } catch (BCDatabaseException ex) {
                Logger.getLogger(BookHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, PREFIX, ex);
        }
        return null;
    }

    private ItemStack setItemStackName(ItemStack is, String name) {
        ItemMeta m = is.getItemMeta();
        m.setDisplayName(name);
        is.setItemMeta(m);
        return is;
    }
}
