/*
 * Copyright (C) 2013-2014 Bernhard Geisberger, Yannik Korzikowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.bibercraft.bcpostamt.books;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

/**
 *
 * @author NickDaKing, goasi
 */
public class Book {

    private String author;
    private String title;
    private Date date;
    private final List<String> pages;

    public Book(org.bukkit.inventory.ItemStack bookItem) {
        Calendar calendar = new GregorianCalendar();

        BookMeta bookMeta = (BookMeta) bookItem.getItemMeta();

        this.author = bookMeta.getAuthor();
        this.title = bookMeta.getTitle();
        this.date = calendar.getTime();
        this.pages = bookMeta.getPages();
    }

    public Book(String title, String author, List<String> pages) {
        this.title = title;
        this.author = author;
        this.pages = pages;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getPages() {
        return this.pages;
    }

    public String getText() {
        StringBuilder result = new StringBuilder();
        for (String page : pages) {
            result.append("<!>").append(page);
        }
        return result.toString().substring(3, result.length());
    }

    public String getPage(int page) {
        return this.pages.get(page);
    }

    public Date getDate() {
        return this.date;
    }

    public org.bukkit.inventory.ItemStack unsign() {
        ItemStack generateItemStack = generateItemStack(1);
        generateItemStack.setType(Material.BOOK_AND_QUILL);
        return generateItemStack;
    }

    public org.bukkit.inventory.ItemStack generateItemStack(int amount) {
        ItemStack is = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) is.getItemMeta();
        bookMeta.setAuthor(author);
        bookMeta.setTitle(title);
        bookMeta.setPages(pages);
        is.setItemMeta(bookMeta);
        is.setAmount(amount);

        return is;
    }
}
